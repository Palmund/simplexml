package net.palmund.simplexml;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by palmund on 05/03/14.
 */
public class TestXmlFormatter {
    private XmlDocument document;
    private XmlFormatter formatter;

    @Before
    public void setUp () throws Exception {
        document = new XmlDocument();
        formatter = new XmlFormatter();
    }

    @Test
    public void asXml_SingleNode () throws Exception {
        document.setRoot(new XmlNode("root"));

        final String formattedDocument = formatter.formatDocument(document);

        Assert.assertEquals(formattedDocument, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<root/>\n");
    }

    @Test
    public void asXml_NodeWithValue () throws Exception {
        document.setRoot(new XmlNode("root", "value"));

        final String xml = formatter.formatDocument(document);

        Assert.assertEquals(xml, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                                    "<root>value</root>\n");
    }

    @Test
    public void asXml_NodeWithAttributes () throws Exception {
        document.setRoot(new XmlNode("root", "value") {{
            addAttribute(new XmlAttribute("attribute", "value"));
        }});

        final String xml = formatter.formatDocument(document);

        Assert.assertEquals(xml, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                                 "<root attribute=\"value\">value</root>\n");
    }

    @Test
    public void asXml_NodeWithChildren () throws Exception {
        document.setRoot(new XmlNode("root") {{
            addChild(new XmlNode("child1"));
        }});

        final String xml = formatter.formatDocument(document);

        Assert.assertEquals(xml, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                                    "<root>\n" +
                                    "    <child1/>\n" +
                                    "</root>\n");
    }

    @Test
    public void asXml_NodeWithNamedNamespace () throws Exception {
        document.setRoot(new XmlNode("root") {{
            setNamespace(new XmlNamespace("local", "http://localhost"));
        }});

        final String xml = formatter.formatDocument(document);

        Assert.assertEquals(xml, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                                    "<local:root xmlns:local=\"http://localhost\"/>\n");
    }

    @Test
    public void asXml_NodeWithUnnamedNamespace () throws Exception {
        document.setRoot(new XmlNode("root") {{
            setNamespace(new XmlNamespace(null, "http://localhost"));
        }});

        final String xml = formatter.formatDocument(document);

        Assert.assertEquals(xml, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                                    "<root xmlns=\"http://localhost\"/>\n");
    }

    @Test
    public void asXml_DocumentIsStandalone () throws Exception {
        document.setRoot(new XmlNode("root") {{
        }});
        document.setStandalone(true);

        final String xml = formatter.formatDocument(document);

        Assert.assertEquals(xml, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                                    "<root/>\n");
    }

    @Test
    public void asXml_NodeWithComment () throws Exception {
        final XmlNode node = new XmlNode("testNode");
        node.setComment("This is a comment!");
        document.setRoot(node);

        final String xml = formatter.formatDocument(document);

        Assert.assertEquals(xml, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                                   "<testNode>\n" +
                                    "    <!--This is a comment!-->\n" +
                                    "</testNode>\n");
    }
}