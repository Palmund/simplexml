package net.palmund.simplexml;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by palmund on 05/03/14.
 */
public class TestXmlNode {
    @Test
    public void testGetAttributes () throws Exception {
        final XmlNode node = new XmlNode("testNode") {{
            addAttribute(new XmlAttribute("attribute2", "value"));
            addAttribute(new XmlAttribute("attribute1", "value"));
        }};

        Assert.assertEquals("Node does not have exactly 2 children", 2, node.getAttributes().size());
    }

    @Test
    public void testGetChildren () throws Exception {
        final XmlNode node = new XmlNode("testNode") {{
            addChild(new XmlNode("child1"));
            addChild(new XmlNode("child2"));
        }};

        Assert.assertEquals("Node does not have exactly 2 children", 2, node.getChildren().size());
    }
}
