/*
 * Copyright (c) 2014. Søren Palmund
 *
 * This file is part of project "SimpleXml".
 *
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.simplexml;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

/**
 * Created by Søren Palmund on 05/03/14.
 */
public class TestXmlDocument {
    @Test
    public void testSimpleDocument () throws Exception {
        final String inputXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<root/>";
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(inputXml.getBytes());
        final XmlDocument xmlDocument = XmlDocument.parse(inputStream);

        final XmlNode root = xmlDocument.getRoot();

        Assert.assertEquals("root", root.getName());
    }

    @Test
    public void parse_SingleNodeWithChildren () throws Exception {
        final String inputXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<root>\n" +
                "   <child1/>" +
                "</root>";
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(inputXml.getBytes());
        final XmlDocument xmlDocument = XmlDocument.parse(inputStream);

        final XmlNode root = xmlDocument.getRoot();
        final XmlNode child = root.getChildren().get(0);

        Assert.assertEquals("root", root.getName());
        Assert.assertEquals(1, root.getChildren().size());
        Assert.assertTrue(child != null);
    }

    @Test
    public void parse_SingleNodeWithValue () throws Exception {
        final String inputXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<root>value</root>";
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(inputXml.getBytes());
        final XmlDocument xmlDocument = XmlDocument.parse(inputStream);

        final XmlNode root = xmlDocument.getRoot();

        Assert.assertEquals("root", root.getName());
        Assert.assertEquals("value", root.getValue());
    }

    @Test
    public void parse_SingleNodeWithAttributes () throws Exception {
        final String inputXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<root attribute=\"value\"/>";
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(inputXml.getBytes());
        final XmlDocument xmlDocument = XmlDocument.parse(inputStream);

        final XmlNode root = xmlDocument.getRoot();
        final XmlAttribute attribute = root.getAttributes().get(0);

        Assert.assertEquals("root", root.getName());
        Assert.assertEquals(1, root.getAttributes().size());

        Assert.assertEquals("attribute", attribute.getName());
        Assert.assertEquals("value", attribute.getValue());
    }

    @Test
    public void parse_SingleNodeWithComment () throws Exception {
        final String expectedComment = "This is a comment";

        final String inputXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<root><!-- " + expectedComment + " --></root>";
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(inputXml.getBytes());
        final XmlDocument xmlDocument = XmlDocument.parse(inputStream);

        final XmlNode root = xmlDocument.getRoot();
        final String comment = root.getComment();

        Assert.assertEquals("root", root.getName());
        Assert.assertEquals(comment, expectedComment);
    }

    @Test
    public void parse_DocumentIsStandalone () throws Exception {
        final String expectedComment = "This is a comment";

        final String inputXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                            "<root><!-- " + expectedComment + " --></root>";
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(inputXml.getBytes());
        final XmlDocument xmlDocument = XmlDocument.parse(inputStream);

        final XmlNode root = xmlDocument.getRoot();
        final String comment = root.getComment();
        final boolean isStandalone = xmlDocument.isStandalone();

        Assert.assertEquals("root", root.getName());
        Assert.assertEquals(comment, expectedComment);
        Assert.assertEquals(true, isStandalone);
    }

    @Test
    public void asXml_SingleNode () throws Exception {
        final XmlDocument document = new XmlDocument();
        document.setRoot(new XmlNode("root"));

        final String xml = document.asXml();

        Assert.assertEquals(xml, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                                    "<root/>\n");
    }
}