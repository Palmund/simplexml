/*
 * Copyright (c) 2014. Søren Palmund
 *
 * This file is part of project "SimpleXml".
 *
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.simplexml;

/**
 * This class represents an attribute on an {@link net.palmund.simplexml.XmlNode}.
 */
public class XmlAttribute {
    private final String name;
    private final String value;

    public XmlAttribute (String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName () {
        return name;
    }

    public String getValue () {
        return value;
    }
}