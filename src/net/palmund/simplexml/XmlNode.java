/*
 * Copyright (c) 2014. Søren Palmund
 *
 * This file is part of project "SimpleXml".
 *
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.simplexml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class represents an XML element that can have attributes and children.
 */
public class XmlNode {
    private final List<XmlAttribute> attributes = new ArrayList<XmlAttribute>();
    private final List<XmlNode> children = new ArrayList<XmlNode>();

    private String name;
    private String value;
    private XmlNamespace namespace;
    private String comment;

    public XmlNode (String name) {
        this(name, "");
    }

    public XmlNode (String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getValue () {
        return value;
    }

    public void setValue (String value) {
        this.value = value;
    }

    public List<XmlAttribute> getAttributes () {
        return Collections.unmodifiableList(attributes);
    }

    public List<XmlNode> getChildren () {
        return Collections.unmodifiableList(children);
    }

    public XmlNamespace getNamespace () {
        return namespace;
    }

    public void setNamespace (XmlNamespace namespace) {
        this.namespace = namespace;
    }

    public String getComment () {
        return comment;
    }

    public void setComment (String comment) {
        this.comment = comment;
    }

    public void addChild(XmlNode child) {
        children.add(child);
    }

    public void addAttribute(XmlAttribute attribute) {
        attributes.add(attribute);
    }
}