/*
 * Copyright (c) 2014. Søren Palmund
 *
 * This file is part of project "SimpleXml".
 *
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.simplexml;

/**
 * This class represents a namespace on an {@link net.palmund.simplexml.XmlNode}.
 */
public class XmlNamespace {
    private String name;
    private String uri;

    public XmlNamespace (String name, String uri) {
        this.name = name;
        this.uri = uri;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getUri () {
        return uri;
    }

    public void setUri (String uri) {
        this.uri = uri;
    }
}