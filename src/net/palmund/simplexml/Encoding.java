/*
 * Copyright (c) 2014. Søren Palmund
 *
 * This file is part of project "SimpleXml".
 *
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.simplexml;

/**
 * This enum represents each of the supported encoding in XML.
 */
public enum Encoding {
    UTF8("UTF-8"),
    UTF16("UTF-16");

    private final String encoding;

    Encoding (String encoding) {
        this.encoding = encoding;
    }

    public String getEncoding () {
        return encoding;
    }
}