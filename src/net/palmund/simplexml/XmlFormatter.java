/*
 * Copyright (c) 2014. Søren Palmund
 *
 * This file is part of project "SimpleXml".
 *
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.simplexml;


import org.w3c.dom.Document;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

class XmlFormatter {
    public String formatDocument (XmlDocument xmlDocument) {
        final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        final OutputStreamWriter streamWriter;

        try {
            final String encoding = xmlDocument.getEncoding().getEncoding();
            final W3CAssembler assembler = new W3CAssembler();
            final Document document = assembler.assembleDocumentUsingXmlNodeAsRoot(xmlDocument.getRoot());
            document.setXmlStandalone(xmlDocument.isStandalone());
            final DOMSource source = new DOMSource(document);
            final Transformer transformer = getXmlTransformer(xmlDocument);
            streamWriter = new OutputStreamWriter(byteStream, encoding);
            transformer.transform(source, new StreamResult(streamWriter));
        }
        catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        catch (TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        }
        catch (TransformerException e) {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return new String(byteStream.toByteArray());
    }

    private Transformer getXmlTransformer (XmlDocument document) {
        Transformer transformer = null;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.STANDALONE, document.isStandalone() ? "yes" : "no");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        }
        catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        catch (TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        }
        return transformer;
    }
}