/*
 * Copyright (c) 2014. Søren Palmund
 *
 * This file is part of project "SimpleXml".
 *
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.simplexml;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class represents an XML document having a root node.
 *
 * @author S&oslash;ren Palmund
 */
public class XmlDocument {
    private static final XmlDocumentFactory factory = new XmlDocumentFactory();

    public static XmlDocument parse(InputStream inputStream) throws IOException, SAXException, ParserConfigurationException {
        return factory.parse(inputStream);
    }

    private XmlNode root;
    private Encoding encoding;
    private boolean isStandalone;

    public XmlDocument () {
        this.encoding = Encoding.UTF8;
    }

    public XmlNode getRoot () {
        return root;
    }

    public void setRoot (XmlNode root) {
        this.root = root;
    }

    public Encoding getEncoding () {
        return encoding;
    }

    public void setEncoding (Encoding encoding) {
        this.encoding = encoding;
    }

    public boolean isStandalone () {
        return isStandalone;
    }

    public void setStandalone (boolean isStandalone) {
        this.isStandalone = isStandalone;
    }

    public String asXml() {
        final XmlFormatter formatter = new XmlFormatter();
        return formatter.formatDocument(this);
    }

    private static class XmlDocumentFactory {
        private final DocumentBuilderFactory documentBuilderFactory;

        private XmlDocumentFactory () {
            documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
        }

        public XmlDocument parse (InputStream inputStream) throws ParserConfigurationException, IOException, SAXException {
            final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            final Document parsedDocument = documentBuilder.parse(inputStream);

            final Element rootElement = parsedDocument.getDocumentElement();
            final XmlNode rootNode = createXmlNodeFromW3CNode(rootElement);
            final XmlDocument xmlDocument = new XmlDocument();
            final boolean isStandalone = parsedDocument.getXmlStandalone();
            xmlDocument.setStandalone(isStandalone);
            xmlDocument.setRoot(rootNode);
            return xmlDocument;
        }

        private XmlNode createXmlNodeFromW3CNode (Node node) {
            final XmlNode xmlNode = new XmlNode(node.getNodeName());

            //<editor-fold desc="Add attributes">
            final NamedNodeMap attributes = node.getAttributes();
            for (int i = 0; i < attributes.getLength(); i++) {
                final Node attribute = attributes.item(i);
                final String name = attribute.getNodeName();
                final String value = attribute.getNodeValue();
                final XmlAttribute xmlAttribute = new XmlAttribute(name, value);
                xmlNode.addAttribute(xmlAttribute);
            }
            //</editor-fold>

            //<editor-fold desc="Add children">
            final NodeList childNodes = node.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                final Node child = childNodes.item(i);
                switch (child.getNodeType()) {
                    case Node.ELEMENT_NODE:
                        final XmlNode childXmlNode = createXmlNodeFromW3CNode(child);
                        xmlNode.addChild(childXmlNode);
                        break;
                    case Node.TEXT_NODE:
                        final String nodeValue = child.getNodeValue().trim();
                        xmlNode.setValue(nodeValue);
                        break;
                    case Node.COMMENT_NODE:
                        final String nodeComment = child.getNodeValue().trim();
                        xmlNode.setComment(nodeComment);
                        break;
                    default: // empty
                        break;
                }
            }
            //</editor-fold>

            return xmlNode;
        }
    }
}