/*
 * Copyright (c) 2014. Søren Palmund
 *
 * This file is part of project "SimpleXml".
 *
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.simplexml;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Søren Palmund on 05/03/14.
 */
class W3CAssembler {
    private final Document document;

    W3CAssembler () throws ParserConfigurationException {
        this.document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    }

    private Element makeW3CElementFromNode (XmlNode node) {
        final Element element = createElement(node);

        //<editor-fold desc="Add node value">
        final String nodeValue = node.getValue();
        element.setTextContent(nodeValue);
        //</editor-fold>

        //<editor-fold desc="Add comment">
        final String nodeComment = node.getComment();
        if ((nodeComment != null) && ! "".equals(nodeComment)) {
            final Comment comment = document.createComment(nodeComment);
            element.appendChild(comment);
        }
        //</editor-fold>

        //<editor-fold desc="Add attributes">
        for (XmlAttribute attribute : node.getAttributes()) {
            final String name = attribute.getName();
            final String value = attribute.getValue();
            element.setAttribute(name, value);
        }
        //</editor-fold>

        //<editor-fold desc="Add children">
        for (XmlNode child : node.getChildren()) {
            final Node assembledChild = makeW3CElementFromNode(child);
            element.appendChild(assembledChild);
        }
        //</editor-fold>

        return element;
    }

    private Element createElement (XmlNode node) {
        final String tagName = node.getName();
        final Element element;
        final XmlNamespace namespace = node.getNamespace();
        if (namespace == null) {
            element = document.createElement(tagName);
        } else if (namespace.getName() == null) {
            element = document.createElement(tagName);
            element.setAttribute("xmlns", namespace.getUri());
        } else {
            element = document.createElementNS(namespace.getUri(), namespace.getName() + ":" + tagName);
        }
        return element;
    }

    public Document assembleDocumentUsingXmlNodeAsRoot (XmlNode root) {
        final Element assembledRoot = makeW3CElementFromNode(root);
        document.appendChild(assembledRoot);
        return document;
    }
}